FROM caddy:2.6.4-builder AS builder

RUN xcaddy build \
    --with github.com/caddyserver/cue-adapter

FROM androw/caddy-security
FROM caddy:alpine

COPY Caddyfile /etc/caddy/Caddyfile

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
